# The ehprog book

This is the canonical initial guide and reference to ehprog.  This is where you should start if you want to learn
ehprog or are interested in contributing to the language.
