# Good practices
## Control flow

The standard library provides standard control-flow macros like `if`, `when`, `for` or `while`, but these should
currently be avoided, because hand-written control structs can and usually are more efficient.  Take a look at this
naive `filter` implementation:
```haskell
let rec filter $(f :: Closure Bool Any) (Cons $fst $rest) =
    if (f fst) then:
        ^Cons fst (filter f rest)
    else:
        filter f rest
```

This creates an additional multi-function with two implementations every loop.  This can be improved.  In comparison,
this is how the standard library currently implements filter.
```haskell
def fun filter $(f :: Closure Bool Any) $(lst :: List) =
    let rec inner $(f :: Closure Bool Any) `() = `();
    let rec inner $(f :: Closure Bool Any) (Cons $fst $rest) =
        inner (f fst) f (^Cons fst rest);;
    let rec inner ~^True $(f :: Closure Bool Any) (Cons $fst $rest) =
        ^Cons fst (inner f rest);;
    let rec inner ~^False $(f :: Closure Bool Any) (Cons $fst $rest) =
        inner f rest;;
    inner f lst;;
```
Here the functionality of the `if` has been condensed into two implementations of the inner recursive function.  This
implementation, although still naive, doesn't create a multi-function in every loop and additionally tail-calls two out
of three times.

## `let` and `def`

`def` should be used for functions and variables spanning multiple other definitions and for publicly available atoms.
For everything else `let` should be used.  The way they are captured when in a free-variable position is very different.
`def` definitions are stored in a hash map that is then stored in the closure.  `let` definitions are saved in a dense
array in that same closure.

## Stringly-typed programming

Stringly-typed or, in the case of ehprog, atomically-typed programming should be avoided.  That means instead of using
atoms (or strings) for enumeration, you should use a sum type of unit types.  To put that in other terms:
```haskell
-- use this:
type ^Foo is:
    | (Bar);
    | (Baz);
    | (Qux);
^Foo.Bar;
^Foo.Baz;
^Foo.Qux;

-- instead of this
`foo.bar;
`foo.baz;
`foo.qux;
```

## Don't use `Nil` as the absence of a value

`Nil` is not meant to and shouldn't be used to express the absence of a value.  Instead you should use the standard
`Maybe` type provided by the core library.
```haskell
-- do this:
let some = Just 42;
let none = Nothing;

-- instead of:
let some = 42;
let none = Nil;
```

## Don't use `rec` at the top-level for functions which are meant to be extended

If you plan on having your function being extended with new implementations, do not use `def rec`, unless you
specifically want every other implementation to be recursive.  That's because `def rec` and `def fun` are not
intermixable.  So instead define a function using a `def fun` and use an inner `let rec` for your recursion purposes.
This can be seen with the `filter` implementation from the core library, but let's have a look at some other example
too.  `rev` is another function from the core library.  It is used to reverse a collection.  First, a naive
implementation.
```haskell
def rec inner $acc `() = acc;
def rec inner $acc (List $fst $rest) = inner (^Cons fst acc) rest;
```
This forces every other implementation of `rev` to be recursive.  Note that a `def rec` doesn't have to be recursive,
but `rec` and `fun` should be used descriptively.  Notice that using this pattern below also allows you to hide the
initial state of the accumulator parameter.
```haskell
def fun rev $(lst :: List) =
    let rec inner $acc `() = acc;
    let rec inner $acc (List $fst $rest) = inner (^Cons fst acc) rest;
    inner `() lst;;
```
