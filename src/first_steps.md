# First steps

To quickly get started with ehprog, we will write a slightly more complicated "Hello, world!" than you might be used to,
so you can immediately get a taste of what ehprog looks like in practice.

After we've written this first example, we will step by step go through everything we wrote with some simpler examples
and I will explain everything on the way.  Through this process of explaining I will strip away everything you might've
learned from static languages like Rust or C++ and teach you the concepts of ehprog, an (extremely) dynamic language.

With that said, here's the most complicated program ever written in ehprog.  A naive quicksort implementation, that uses
nearly every feature ehprog has to offer at this point.

```haskell
from core import < >=;

-- chooses one of two compound expressions based on its input
macro if = [ macro | $p then: $t else: $f | 
    let func = macro.gensym ();
    `([ | (True) | ~t
        | (False) | ~f ] ~p)
];

-- reverses a list
let fun rev $(list :: List) =
    let rec inner $acc () = acc;
    let rec inner $acc (List $head $rest) =
        inner (^Cons head acc) rest;;
    inner () list;;

-- concatenates two lists
let fun ++ $(lhs :: List) $(rhs :: List) =
    let rec inner $acc () = acc;
    let rec inner $acc (List $last $rest) =
        inner (^Cons last acc) rest;;
    inner rhs (rev lhs);;

-- applies a closure to every element in the list and only keeps those, where the closure returned True
let rec filter $_ () = ();
let rec filter $(f :: Closure Bool Any) (List $fst $rest) =
    if (f fst) then:
        ^Cons fst (filter f rest)
    else:
        filter f rest;;

-- creates a new list that is the sorted version of the input
let rec qsort () = ();
let rec qsort (List $pivot $tail) =
   let left = filter [| $x | x < pivot ] tail;
   let right = filter [| $x | x >= pivot ] tail;
   (qsort left) ++ (^Cons pivot (qsort right));;

qsort `(8 2 7 4 0 1 8 7 4 7 2 9 2 6)
```
