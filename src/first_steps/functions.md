# Functions are everything

You know those languages that claim that "everything is a function"?  I like the idea that instead in ehprog functions
are everything.  How does that work?  Functions actually creep into every part of your workflow in ehprog.  Because of
the very powerful pattern matching, functions can be used where other languages might need specialized control flow
structures.

Let's outline some characteristics of functions in ehprog. 
- Every function is a closure.  
  - Free variables are closed over automagically.
  - Closures can be dynamic or lexical.
- Every function is a multi-function.
  - A correct implementation will be chosen depending on the pattern in the parameter list.
- Functions are first-class and higher-order.
- Tail-recursive functions are declared explicitly, but used implicitly.
- Function calls must be explicit, i.e. a function with zero arguments cannot be called.

## Function calls

Applications (function calls) are explicit.  An application has the form of `f a1 ... an`, where `a1` through an `are`
the arguments or _proper parameters_.  `f` is the function called, or in other words, the function to which the
arguments are applied.  Applications also have a second form.  You've seen this already before in expressions like
`1 + 2`.  ehprog supports a variation of uniform function call syntax (ufcs for short).  A regular function can always
appear in the second position.  In an expression of the form `f g`, if both `f` and `g` are functions, `f` will be
called with `g`, not the other way around.

## Function definitions

The canonical way to define a closure is with the closure syntax: `[ [dyn [mut]] [macro] | <patterns>.. | <expr>.. ]`.
Here `<patterns>..` is a list of patterns, or parameters, and `<expr>..` is any expression, or list of words.  For
example `[| $x | x ]` defines the identity function.  `[| $a $b $c | a + b ]` defines a function that adds two of its
parameters and discards the third.  Note that `[`, `|` and `]` are not words; they're part of the closure literal.  So
you don't have to parenthesize patterns or the function body. 

## If

Internally (how this works precisely, we'll discuss later) an `if` is defined as a function with two implementation.
`if p t f` generates something along the lines of the following code.

```haskell
let fun __gen__0 ^True = t;
let fun __gen__0 ^False = f;
__gen__0 p
```

This code creates a function with two implementations and immediately calls it with a single argument.

## Artifacts

There are some side-effects of the semantical word and function rules.  Namely, complex arithmetical operations must be
explicitly parenthesized.  So you can't write `1 + 2 * 3`, because that would call the function `+` with the arguments
`1`, `2`, `*` and `3` and that's _undefined_ (undefined as in it's an error, not as in undefined behaviour).  So instead
write `1 + (2 * 3)`.
