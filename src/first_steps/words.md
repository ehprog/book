# ehprog is a word based language

We've talked about integers, and how to perform the most basic operations on them.  To do that we used some operators
like `+`, `-` and so on.

But I didn't mention a tiny detail.  Namely, these are not operators.  In fact, they're not keywords or even reserved
words.  ehprog doesn't have any keywords and less than a handful of contextual reserved words.  `+` is a function that
directly maps to the builtin function `intrinsic.add`.  Here's the actual definition of `+` straight from the standard
library.

```haskell
let fun + $(a :: Int) $(b :: Int) = intrinsic.add a b;
let fun + $(a :: Ratio) $(b :: Ratio) = intrinsic.add a b;
let fun + $(a :: Real) $(b :: Real) = intrinsic.add a b;
```

That's a lot of scary symbols, but what this basically means is that `+` is a function which can add either two `Int`s,
two `Ratio`s or two `Real`s.  We'll talk later about what `$` and `let fun` mean.

`+`, `a`, `1` are all words.  Words, just like words in written and spoken languages, must be explicitly delimited.  In
practice this means that you cannot write `1+2`, because `1` is a word just like `+`.  They must be delimited `1 + 2`,
because otherwise the lexer would get very confused and spit out errors.  Words have different types and unlike english
words, aren't always atomic.  `a` and `+` both have the type `Atom`. `1` has the type `Int`, outlined in [the previous
chapter](./ints.md).

ehprog is a word based language.  (Almost) everything is a word of some sort.  However word in itself is not a type or
some sort of super-supertype that some languages provide (like `Object` in Java).  A word is just an in-source
representation of some value.  Values are the real words that ehprog is based on.  Again, there is no `Value`
super-supertype.  There is the `Any` type, but that's a topic for a later chapter.

## Word kinds

Let's review some common word kinds.

- Nil — Nil doesn't have a primitive notation, but can be expressed in one of two ways.  Using the constructor, `^Nil`,
   or the empty expression, `()`.  `()` is the preferred notation for Nil in code.
- Atom — `foo`, `bar`, `baz`, etc.  A raw atom will be evaluated as a binding, so to use an atom as a literal, use a
   quote (`\foobar`) or (preferably) a backtick (`` `bazqux ``).
- Int — `0`, `42`, `-42`, `123abcxyz:36`.  The `:36` notation is used to denote the base of the integer.  `123abcxyz:36`
   is an integer in base 36.  These are 64-bit machine integers.
- Ratio — `1/2`, `1/1`, `-4/2`.  Machine-precision rational numbers.  Represented as `1:64:64`, i.e. 1 sign-bit, 64
   numerator bits, 64 denominator bits.
- Real — `0.0`, `-1.0`, `4.2`.  These are the IEEE-754 floating point numbers.  Currently the scientific notation
   (`4.2e+2`) is unsupported.
- Char — `'a'`, `'\n'`, `'\0'`, `'\''`, `'"'`.  Currently there is no support for the unicode scalars (`\u0061`) or hex
   (`'\x61'`) notation.
- Str (pronounced string) — `"foobar"`, `"42"`, `"~(21 * 2)"`.  Strings support the same escape sequences as characters,
   with the exception of `'\''` and `'"'`.  These are represented as `"'"` and `"\""` respectively.  Strings support
   interpolation.
- constructors (`^`) — `^Foo` refers to the constructor for the type `Foo`.  Unlike regular functions, constructors can
   be called without arguments.  Common constructors include `^Nil`, `^True`, `^False`, `^Cons $head $(tail :: List)`
   and `^Empty`.  To instead refer to the underlying function, use a quote or a backtick: `\^Nil` or `` `^Nil ``
- quotations (`\`) — As shown with atoms and constructors, quotations `\` can be used to escape some expressions that
   would otherwise be evaluated.  This can be used for example to use atom, constructor list literals.  
- quasi-quotation (`` ` ``) — Regular quotation will prevent anything inside an expression from being evaluated.
   Sometimes it can be useful to allow some parts to be evaluated, for example when creating lists using the shorthand
   notation: `` `(1 2 3 4) ``
- interpolation (`~`) — To actually allow something to be evaluate inside a quasi-quotation, it needs to be interpolated
   like so: `` `(~foobar ~(21 * 2) ~^True) ``.
- empty expression — The empty expression `()` can be used to express two different things: when evaluated it is equal
   to `^Nil` and when quoted with `\()` or `` `() `` it is equal to `^Empty` (the empty list).
- compound — The compound word is the last word we'll discuss in this chapter.  It can be used to combine multiple
   expressions into a single word.  The statements will be executed one-by-one in order, and the optional expression
   will be the return value.  If not provided, the compound word will return Nil.  In this example everything following
   the first `=` is the compound word.
   ```
   let foo =
       let x = 1 + 2;
       println "~(x)";
       x
   ```

Words are used for everything from lists through function parameters to anonymous functions.  _Functions_.

Let's talk about functions in the next chapter.
