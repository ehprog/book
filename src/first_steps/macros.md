# Macros

The last little bit of syntax and semantics we need to discuss to understand the initial example is macros.  A macro is
used to execute code at compile-time and because ehprog is homoiconic, they're also used to generate code, at
compile-time.  A macro definition has a simple form:

```haskell 
macro foobar = [| $(a :: Int) $(b :: Int) | a + b ];
foobar 1 2
```

This macro adds two integers at compile-time.  Let's analyze the `if` macro from the initial example.

```haskell
macro if = [ macro | $p then: $t else: $f | 
    let func = macro.gensym ();
    `([ | (True) | ~t
        | (False) | ~f ] ~p)
];
```

First we generate a name for the multi-function that will be used for pattern matching: `let func = macro.gensym ();`.
`macro.gensym` is a compile-time function that generates an atom that can later be used to refer to a binding
consistently and semi-hygienically.

The next part is a quoted compound word.  `func` is interpolated — that is how we use an atom generated with `gensym`.
The next word is a destructure pattern.  You can read more about it in [Constructors](./cons.md#destructuring).

```haskell
`([ | (True) | ~t
    | (False) | ~f ] ~p)
```

```haskell
if p then:
    println "In the true branch.";
    1
else:
    println "In the false branch.";
    0
-- expands to
([ | (True) |
    println "In the true branch.";
    1
   | (False) |
    println "In the false branch.";
    0 ] p)
```

And that is all the basic tools we need to understand macros.  And with that we have all the basic tools needed to
understand the initial example strapped to our tool belt.  In the next chapter you will learn how to use them in a good
fashion.
