# The let family

`let` is the closest thing you'll find to a keyword in ehprog.  It's a reserved contextual word that gets special
handling from the compiler.  It exists in three forms, which will be explained in detail in this section of the book.

## `let` bindings

The most basic form of `let` is `let <binding> = <expr>..`, where `<binding>` is any atom and `<expr>..` is a list of
words which will be evaluated as a seperate expression.  This form binds the value of `<expr>..` to the atom
`<binding>`.  These bindings are not global and will therfore not be visible outside of modules (more on modules later).
Let expressions always evaluate to `Nil`, the unit type.

Let's see some examples of `let`.

```haskell
let a = 5; -- a starting with the next expression, a will evaluate to 5
let x = 5.0; -- similarly, x will evaluate to 5.0
foo; -- foo is undefined here, bindings are evaluated in order
let foo = `a; -- foo is now defined
let bar = 6 + (6 * 6);
let id = [| $x | x ]; -- the identity function
```

You might've noted that a whole expression is bound to `bar`, not just a single word.  This would be trivial in any
other language, but due to ehprogs handling of expressions, this shouldn't work.  But it doesn't.  `let` is one of the
few magical words in ehprog that _just work_ and make the programmers life simpler.  This includes doing things that
wouldn't normally be possible with the semantical rules of ehprog.

In ehprog, all bindings are immutable, but can be shadowed.  This means that you can't reassign a binding like in e.g.
C: `let x = 5` and later `x = 5`, but you can shadow an older binding by providing a new definition:  `let x = 5` and
later `let x = 42`.

## `let fun` bindings

The second form of `let` we'll take a look at is `let fun <binding> <patterns>.. = <expr>..`.  This use of `let` is even
more magical than the previous.  `let fun` creates a new multi-function that can later at any point be extended with new
implementations.  `<expr>..` is some expression, just like in the regular `let` binding.  `<patterns>..` is a list of
patterns.  We'll get to patterns at a later point in this book.  For now you can treat them as parameters.  Let's take a
look at some examples.

```haskell
let fun f $x = x; -- f is an identity function
let a = 5.0; -- a is an immutable binding
let b = 2.0; -- a is an immutable binding
let fun g $x = (a * x) + b; -- g closes over its environment just like any other closure

let fun foo $(a :: Int) = println "my parameter is an Int";
let fun foo $(a :: Real) = println "my parameter is a Real";
let fun foo $a = println "my parameter is something else";

foo 1; -- prints "my parameter is an Int"
foo 1.0; -- prints "my parameter is a Real"
foo "1"; -- prints "my parameter is something else"

-- can we sum a list using functions?
let fun sum $acc () = acc;
let fun sum $acc (Cons $fst $rest) = sum (acc + fst) rest;
-- nice try at a sum function! but invalid!
```

Why is this `sum` function invalid?  It's because `let fun` forbids self-references.  This isn't a technical limitation,
it's a design choice.  `let fun` was explicitly defined to not allow self-referencing functions.  Self-referencing
functions are defined with `let rec`.

## `let rec` bindings

The third and last form of `let` is `let rec <binding> <patterns>.. = <expr>..`.  It works just like `let fun`, with the
only two differences being:
- `let rec` functions can reference (and therefore call) themselves
- applications of self in the last position will be tail-called

`let rec` can therefore be used for looping and generally for recursion (efficiently, that is).

```haskell
-- this works!
let rec sum $acc () = acc;
let rec sum $acc (Cons $fst $rest) = sum (acc + fst) rest;
```

Note this however: `let rec` and `let fun` cannot be mixed.  You can't provide one implementation that's recursive and
one that's non-recursive.  Good practices on how to use and mix `let fun` and `let rec` will be explained in [Good
practices](../practice.md).
