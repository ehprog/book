# Various patterns

We've reviewed some word kinds and their contextes.  There's one more context in which words behave slightly differently
than others.  That is as patterns, or parameters.  When words appear in the parameter list of an anonymous function or
one of `let fun`, `let rec`, `def fun`, `def rec` bindings, they are called parameters and are compiled into slightly
different code.  Most words act as if they were quasi-quoted.  The notable notation is `()`.  It's still equal to
`^Nil`.  The reasoning behind this is that functions cannot be called without any arguments.  To avoid that, we use
functions that take a single parameter — Nil.  These are then called like `foobar ()`.  To remain consistent in the
ideology of declarative function definitions, the declaration also uses a `()` instead of the ugly `~^Nil`.  So the
following defines and uses a function of "zero" parameters instead of a function taking the empty list.
```haskell
let fun foobar () = `foobar;
foobar ()
```

There's 2 kinds of words we haven't visited yet, but have seen in use.

- Bind — `$x`, `$(foo :: Bar)`, `$(qux :: Quz : corge?)`.  These can be used to bind a value to an atom.  Additional
   predicates can be used to constrain the values.  1) A type can be used to constrain the type of a value and 2) a
   predicate can be use further constrain the values.  A constrain can also be used without a type.
- Destr (pronounced destructure) — `(Cons $fst $rest)`, `(Foo $x $y $z)`, `(True)`.  A Destr is used to destructure a
   value into its components.  A Cons consists of two parameters, so two parameters appear in the parameter list.  The
   parentheses are obligatory in a Destr.  Notice that there is no caret (`^`) in fron of the type name.  Notice also
   that any other pattern can be used in the parameter list.
