# Constructors

There one special type of functions.  Those are constructors.  A constructor is referred to by its variant name with a
caret in front.  `^Cons head tail` constructs a new `Cons` with two parameters: `head` and `tail`.  This `Cons`
contructor returns a `List`.  Why do we refer to it by `^Cons` instead of `^List`?  That's because `List` has two
variants: `(Empty)` and `(Cons $head $(tail :: List))`.  The former denotes an empty list, the latter denotes an element
followed by another list.

## Destructuring

A list created with `^Cons` can also be destructured in a pattern into its components.  A function
`[| (Cons $fst $rest) | fst ]` would destructure its `List`-type parameter into two sub-patterns: `$fst` and `$rest`.
`fst` will be bound to the first parameter of `Cons` and `rest` will be bound to the second.

## `type`

There is one more use to constructors and destructururing and that is to declare a new type.  We do it like so:

```haskell
type ^List is:
    (Empty);
    (Cons $head $(tail :: List));;
```

This would define a linked list type, which can be used like we described in [Constructors](#constructors) and
[Destructuring](#destructuring).  In fact, this is how `List` will defined in the core library in the future.
