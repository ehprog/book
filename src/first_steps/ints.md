# Let there be ints!

We'll start from the very bottom of our quicksort implementation.  Near the bottom we called quicksort like so:

```haskell
qsort `(1 2 3 4)
```

Although the syntax might look scary at first, the thins between the parentheses might look familiar to you.  They're
regular old 64-bit signed integers.

## Operations

`Int`egers can be added, subtracted, multiplied and divided by each other.  They can also be compared in one of six
different ways.

```haskell
1 + 5 -- 6
7 - 2 -- 5
6 * 7 -- 42
99 / 11 -- 9
3 = 3 -- True
4 /= 3 -- True
2 > 1 -- True
1 < 2 -- True
2 >= 2 -- True
2 <= 2 -- True
```

## Interjection!  A quick note on other numeric types

ehprog also supports two other numeric types.  `Real` and `Ratio`.  `Real` is a IEEE-754 64-bit floating-point number
and behaves as such in every way.  `Ratio` is a 1:64:64 bit rational number (1-bit sign, 64-bit numerator, 64-bit
denominator).  These types cannot be mixed and if one wishes to perform an operation on two different numeric types, a
`promote` operation or an explicit casts must be performed.  The explicit conversions are called `->int`, `->ratio` and
`->real`.
