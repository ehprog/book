# ehprog

## Getting started

Traditionally programming guides start with setting up the environment.  This book does the same.

If you already have Rust installed, you can skip to [Building ehc](#building-ehc).

### Running Rust nightly

1. Install rustup
```
$ curl https://sh.rustup.rs -sSf | sh
```
2. Install the nightly toolchain
```
$ rustup install nightly
```
3. Check to make sure everything was installed properly.
```
$ cargo +nightly -V
$ rustc +nightly -V
```
These should output something along the lines of
```
cargo 1.34.0-nightly (5c6aa46e6 2019-02-22)
rustc 1.34.0-nightly (00aae71f5 2019-02-25)
```

### Installing ehrt
For this you will need clang and GNUMake.  These can be installed from your distros repositories and I'm
gonna assume you know how to achieve this.

1. Clone the ehprog runtime from repository master.
```
$ git clone https://gitlab.com/ehprog/rt
$ cd ./rt
```
2. Build with
```
$ make
```
3. Install ehrt
```
$ make install
```

### Installing ehprt0
Same dependencies as above.

1. Clone ehprt0
```
$ git clone https://gitlab.com/ehprog/ehprt0
$ cd ./rt
```
2. Build with
```
$ make
```
3. Install ehprt0
```
$ make install
```

### Building ehc 
To build ehc you will need LLVM 6.0 development libraries.  These can be installed from your distros repositories.

1. Setup the environment.
You will need the following envvars setup:
```
$ export LD_LIBRARY_PATH=$HOME/.local/lib64
or, if you're on a 32-bit system
$ export LD_LIBRARY_PATH=$HOME/.local/lib
```
2. Clone the ehprog compiler from repository master.

```
$ git clone https://gitlab.com/ehprog/ehc
$ cd ./ehc
```
3. To make sure Rust nightly is always used with ehc, create an override.
```
$ rustup override set nightly
```
4. Build with
```
$ cargo build
```
5. Install ehc
```
$ cargo install --path .
```

### Running ehc
Now you can finally run ehc.  
1. Use ehc to compile a file.
```
$ ehc --emit-{llvm,bc,asm,obj} -O{0,1,2,3} -o path/to/file{.ll,.bc,.s,} -- path/to/file.ehp
```
2. Use ehc to compile the internal core library.
```
$ ehc --emit-{llvm,bc,asm,obj} -O{0,1,2,3} -o path/to/core{.ll,.bc,.s,} --core
```
3. Run the following to get comprehensiv help from the compiler.
```
$ ehc --help
```

If you plan on linking an executable, you will need a `main.ehp` module and ehprt0.  You will also have to compile the
core library yourself.  To link an executable from the following.
```
$ clang -L$HOME/.local/lib64 -lehrt -lm -o main main.o core.o $HOME/.local/lib64/ehprt0.o ...
```
